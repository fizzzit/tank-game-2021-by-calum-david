﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MenuSceneLoader::Awake()
extern void MenuSceneLoader_Awake_m18B4E09AAFDC9578469D066C50A8AC0054AA34AC (void);
// 0x00000002 System.Void MenuSceneLoader::.ctor()
extern void MenuSceneLoader__ctor_m27FD45CA6C6C8B579D2FA4EEDABBA35F2C7EF6BC (void);
// 0x00000003 System.Void PauseMenu::Awake()
extern void PauseMenu_Awake_mB04BCBDA84AEC654D1976EC3DF61A0CF68D2C86C (void);
// 0x00000004 System.Void PauseMenu::MenuOn()
extern void PauseMenu_MenuOn_m8973DC956DD5235381EE4ACB4A182AA5BF0EF2EA (void);
// 0x00000005 System.Void PauseMenu::MenuOff()
extern void PauseMenu_MenuOff_mCD1AF41F916B0C02D2FD6F82377DBEAFF7C30720 (void);
// 0x00000006 System.Void PauseMenu::OnMenuStatusChange()
extern void PauseMenu_OnMenuStatusChange_mD1E8BD9B9CCB274A83FFF0FE2E06E6ABD0361B4A (void);
// 0x00000007 System.Void PauseMenu::Update()
extern void PauseMenu_Update_m191CABDC11442A2CC104FC8B3244D04826E7BD57 (void);
// 0x00000008 System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018 (void);
// 0x00000009 System.Void SceneAndURLLoader::Awake()
extern void SceneAndURLLoader_Awake_m8F276157A2A5FA943EF7918D6CCDB81273317E23 (void);
// 0x0000000A System.Void SceneAndURLLoader::SceneLoad(System.String)
extern void SceneAndURLLoader_SceneLoad_m2B09BD48F419F49A6BD461DBC7B2290EC8632B06 (void);
// 0x0000000B System.Void SceneAndURLLoader::LoadURL(System.String)
extern void SceneAndURLLoader_LoadURL_m47E3E286E80F2D5B3E6A164C32F7E1B473532AE2 (void);
// 0x0000000C System.Void SceneAndURLLoader::.ctor()
extern void SceneAndURLLoader__ctor_m6DEE574FADF9E3E894594690CB2755F69D5D4BE5 (void);
// 0x0000000D System.Void CameraSwitch::OnEnable()
extern void CameraSwitch_OnEnable_mD422991EDD9D880928644EE1BC4E557EE644679C (void);
// 0x0000000E System.Void CameraSwitch::NextCamera()
extern void CameraSwitch_NextCamera_m38AB4521C129032FA1DA5154E09D36D0FE2DB257 (void);
// 0x0000000F System.Void CameraSwitch::.ctor()
extern void CameraSwitch__ctor_m550FCA9B0C24BBBC8BDBCAAAAA7BBF26312399FE (void);
// 0x00000010 System.Void LevelReset::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void LevelReset_OnPointerClick_m9625C8235343CB1DE6B089AD6F2D5ACF738072A4 (void);
// 0x00000011 System.Void LevelReset::.ctor()
extern void LevelReset__ctor_mFD2BB9AF1A9D7E795DDAAE2F33C1F0EB1FB31F07 (void);
// 0x00000012 System.Void AudioManager::Start()
extern void AudioManager_Start_m54C0A7ACBAB2F38052C6B900BBBC3261339662FC (void);
// 0x00000013 System.Void AudioManager::Update()
extern void AudioManager_Update_mC2BFAF2A1E9F96A9BA1C48BBBBB1ED9361E537C8 (void);
// 0x00000014 System.Void AudioManager::ChangeBGM(UnityEngine.AudioClip)
extern void AudioManager_ChangeBGM_m73D374AA2B395FE75D801E3EADCFE34F788381F1 (void);
// 0x00000015 System.Void AudioManager::.ctor()
extern void AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD (void);
// 0x00000016 System.Void Controller::Start()
extern void Controller_Start_mD256D29928E12349C2565BD046108EE5A3537157 (void);
// 0x00000017 System.Void Controller::Update()
extern void Controller_Update_mBC27223743238412D4F6FF7B8B3E3FB2DF47F1A0 (void);
// 0x00000018 System.Void Controller::OnObsticaleEnter(UnityEngine.Collider)
extern void Controller_OnObsticaleEnter_mBA86B4C82A0289681B3F5041889E79DDFD674845 (void);
// 0x00000019 System.Void Controller::.ctor()
extern void Controller__ctor_m50D06C2CAE9B5496780A83ABCF68C529A44EE9F7 (void);
// 0x0000001A System.Void GameBehaviour::Start()
extern void GameBehaviour_Start_m598107700AD6F8F7AFC2FF3C892721AB45757752 (void);
// 0x0000001B System.Void GameBehaviour::Update()
extern void GameBehaviour_Update_m4F6DE76D4E1772CB1D372F80056C7A911001F495 (void);
// 0x0000001C System.Void GameBehaviour::OnCollisionEnter(UnityEngine.Collision)
extern void GameBehaviour_OnCollisionEnter_m81B9B70DD60B5FF20E1BBF24B0F431F0323DE77E (void);
// 0x0000001D System.Void GameBehaviour::OnTriggerEnter(UnityEngine.Collider)
extern void GameBehaviour_OnTriggerEnter_m9105655E75F774B9F6EB92F2959F2DC2AD113887 (void);
// 0x0000001E System.Void GameBehaviour::.ctor()
extern void GameBehaviour__ctor_m3B3CE215E6DE80D656F1AF405CEA4AA8458CDB53 (void);
// 0x0000001F System.Void LevelText::Start()
extern void LevelText_Start_m03232956C7BBDED2C383AD8B7ACADC3E2C672907 (void);
// 0x00000020 System.Void LevelText::OnTriggerEnter(UnityEngine.Collider)
extern void LevelText_OnTriggerEnter_mCF6FE1032791BF97F6B623F059F3F40B9C23715A (void);
// 0x00000021 System.Collections.IEnumerator LevelText::WaitForSec()
extern void LevelText_WaitForSec_m33F5F7E50AFE3688DE6FFB3A7EC06DD0BCFED8D9 (void);
// 0x00000022 System.Void LevelText::Update()
extern void LevelText_Update_mED2776800099C8816820DE02C80C1EAED91C559E (void);
// 0x00000023 System.Void LevelText::.ctor()
extern void LevelText__ctor_m14A5F02DDB0F576F5281310DD7245B2CDBC41FE9 (void);
// 0x00000024 System.Void LevelText/<WaitForSec>d__3::.ctor(System.Int32)
extern void U3CWaitForSecU3Ed__3__ctor_m7B4302C3D8A7CFDF02F534BF5CCDB4D7BCEB4422 (void);
// 0x00000025 System.Void LevelText/<WaitForSec>d__3::System.IDisposable.Dispose()
extern void U3CWaitForSecU3Ed__3_System_IDisposable_Dispose_m287F9796B41BD6A8D01CDCDDC89C9AC78F6EE2D3 (void);
// 0x00000026 System.Boolean LevelText/<WaitForSec>d__3::MoveNext()
extern void U3CWaitForSecU3Ed__3_MoveNext_m24C5DDA54B9822205EF34FB9BCDF50CCC3EE4631 (void);
// 0x00000027 System.Object LevelText/<WaitForSec>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSecU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6953009194EDF55B5668DC359C2F5CE90884E786 (void);
// 0x00000028 System.Void LevelText/<WaitForSec>d__3::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSecU3Ed__3_System_Collections_IEnumerator_Reset_m1ABAF71CE03CF0021D613B1792AEC15BE3246D11 (void);
// 0x00000029 System.Object LevelText/<WaitForSec>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSecU3Ed__3_System_Collections_IEnumerator_get_Current_m36C7A048FBF14E8A3441F42FE1FD344328DC252B (void);
// 0x0000002A System.Void MouseLooker::Start()
extern void MouseLooker_Start_m62C65D9E72654C73653B641DE577A0ED93A3D437 (void);
// 0x0000002B System.Void MouseLooker::Update()
extern void MouseLooker_Update_m9B2049DFCDBC73BA481BA00988231B1011C28E55 (void);
// 0x0000002C System.Void MouseLooker::LockCursor(System.Boolean)
extern void MouseLooker_LockCursor_mB30F16B0D059038F92963F3A9916213DC59A1E70 (void);
// 0x0000002D System.Void MouseLooker::LookRotation()
extern void MouseLooker_LookRotation_mDBD54C8212E2489D0C18C35F277ABE6CDDDC171B (void);
// 0x0000002E UnityEngine.Quaternion MouseLooker::ClampRotationAroundXAxis(UnityEngine.Quaternion)
extern void MouseLooker_ClampRotationAroundXAxis_mCF88B2CD1B917D1E9EFB8275F6D14D63C67D7C56 (void);
// 0x0000002F System.Void MouseLooker::.ctor()
extern void MouseLooker__ctor_m57240C9A9D42D49929076F9A34D4CD8A4E8C9B98 (void);
// 0x00000030 System.Void StartingText::Start()
extern void StartingText_Start_mC3210F7E53E01202B4F67D1E910A2676381C54C4 (void);
// 0x00000031 System.Void StartingText::Update()
extern void StartingText_Update_mF81F252B33E8F5CCA269DA4645390D0125EA1BE3 (void);
// 0x00000032 System.Void StartingText::.ctor()
extern void StartingText__ctor_m6B3571AA22D40B7A19870BCCAC1B828A2FCDA146 (void);
// 0x00000033 System.Void SwitcMusicTrigger::Start()
extern void SwitcMusicTrigger_Start_m0AF0CCE555C762F7C425E8635E7A4994F1A21A67 (void);
// 0x00000034 System.Void SwitcMusicTrigger::Update()
extern void SwitcMusicTrigger_Update_m8391FA6CCF87354CF1AB61E82EC21C2C72D07F13 (void);
// 0x00000035 System.Void SwitcMusicTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void SwitcMusicTrigger_OnTriggerEnter_m8AC6EADB8699B34F93230DE17CFB8BCBC3DD3AFC (void);
// 0x00000036 System.Void SwitcMusicTrigger::.ctor()
extern void SwitcMusicTrigger__ctor_m3108B429117F1F1BDE14236992FA0C4F592D84C5 (void);
// 0x00000037 System.Void Teleport::OnTriggerEnter(UnityEngine.Collider)
extern void Teleport_OnTriggerEnter_mD257053A6E90223E0B71BD2181254C9D4F244017 (void);
// 0x00000038 System.Void Teleport::Start()
extern void Teleport_Start_m09C6BA2D5A92B3DB4BC25D9BAB55557CC3A64E64 (void);
// 0x00000039 System.Void Teleport::Update()
extern void Teleport_Update_mBAF587BC3352BB595BF3C7FBC9E2F036B8628063 (void);
// 0x0000003A System.Void Teleport::.ctor()
extern void Teleport__ctor_mF232DF6C23A42F13C1B57881E696EBA352801E3B (void);
// 0x0000003B System.Void TutorialText::Start()
extern void TutorialText_Start_mD882F1E8E8DD370A4FD4474E9AE41738E16AB501 (void);
// 0x0000003C System.Void TutorialText::OnTriggerEnter(UnityEngine.Collider)
extern void TutorialText_OnTriggerEnter_mEDFA8342059AF1EC688AA926E8D98351B7DDF7D1 (void);
// 0x0000003D System.Collections.IEnumerator TutorialText::WaitForSec()
extern void TutorialText_WaitForSec_mDC285189613F59023A273280C8F9A6768580884E (void);
// 0x0000003E System.Void TutorialText::Update()
extern void TutorialText_Update_m3070918573AECA5D6F7E51C8DF1CB5300A76A022 (void);
// 0x0000003F System.Void TutorialText::.ctor()
extern void TutorialText__ctor_mD0EF3C12D39D80C2E531FC9A1BE098E196B710E9 (void);
// 0x00000040 System.Void TutorialText/<WaitForSec>d__3::.ctor(System.Int32)
extern void U3CWaitForSecU3Ed__3__ctor_m7D06D878043B1BF25D0BF4F2CD7E65C6ED7DCD9A (void);
// 0x00000041 System.Void TutorialText/<WaitForSec>d__3::System.IDisposable.Dispose()
extern void U3CWaitForSecU3Ed__3_System_IDisposable_Dispose_mE7FE26E8AB05EC1423B5F249403F41883A8BD20F (void);
// 0x00000042 System.Boolean TutorialText/<WaitForSec>d__3::MoveNext()
extern void U3CWaitForSecU3Ed__3_MoveNext_m431B15DDDF652E339C2BFCDDCA8E97EF09EEFAED (void);
// 0x00000043 System.Object TutorialText/<WaitForSec>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSecU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B2144C612882471124DAA52DC6D8FB851C545B2 (void);
// 0x00000044 System.Void TutorialText/<WaitForSec>d__3::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSecU3Ed__3_System_Collections_IEnumerator_Reset_m3DBCCB9BDBF62F25BC11015E32689B626953B40D (void);
// 0x00000045 System.Object TutorialText/<WaitForSec>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSecU3Ed__3_System_Collections_IEnumerator_get_Current_m0A71CC4BCF8DCD7244A62D5124A3DD431E0F3983 (void);
// 0x00000046 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Awake()
extern void ParticleSceneControls_Awake_m3268C5B5B81D4EE59C85C27AED5CA3956C284DA4 (void);
// 0x00000047 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::OnDisable()
extern void ParticleSceneControls_OnDisable_mEB5EAF7BC1928EBEFD83E651459327948255D811 (void);
// 0x00000048 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Previous()
extern void ParticleSceneControls_Previous_mC2FE564898A6019238C5611E63B5EC3581CEAA63 (void);
// 0x00000049 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Next()
extern void ParticleSceneControls_Next_m4AB6C18CBE0148C4D354FDCAD5EE45D472577B8E (void);
// 0x0000004A System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Update()
extern void ParticleSceneControls_Update_m7B5894AE70D3733C901BFFF529195AC1467D632B (void);
// 0x0000004B System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::KeyboardInput()
extern void ParticleSceneControls_KeyboardInput_m29C494EC6ACF4EE139686EF47C3A167D1007970F (void);
// 0x0000004C System.Boolean UnityStandardAssets.SceneUtils.ParticleSceneControls::CheckForGuiCollision()
extern void ParticleSceneControls_CheckForGuiCollision_mB8EDC5E34CE1B59986991852009A011D9514A8FB (void);
// 0x0000004D System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Select(System.Int32)
extern void ParticleSceneControls_Select_m123BA8121C7C5C454EF649A13C724F2622B4517A (void);
// 0x0000004E System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.ctor()
extern void ParticleSceneControls__ctor_mFE76B488C636F213B77193EDFF38F514A3186631 (void);
// 0x0000004F System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.cctor()
extern void ParticleSceneControls__cctor_mBA868BA9BC2400AD1AB420B3D0AB42D863358338 (void);
// 0x00000050 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::.ctor()
extern void DemoParticleSystem__ctor_mA046B73C934441755CA5D57991D7C453645AD8CE (void);
// 0x00000051 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList::.ctor()
extern void DemoParticleSystemList__ctor_mAFBB16272EB2E695826211DD030553401DF6E83B (void);
// 0x00000052 System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::Update()
extern void PlaceTargetWithMouse_Update_m28E68FE68FA0185C0D20A6001F4C262868348BE3 (void);
// 0x00000053 System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::.ctor()
extern void PlaceTargetWithMouse__ctor_m3582FA7DC3CF8198244F9EB014FE12AC97985FE7 (void);
// 0x00000054 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::Start()
extern void SlowMoButton_Start_m8F334286C4BEE772EAA4842F431E01BB4929A04C (void);
// 0x00000055 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::OnDestroy()
extern void SlowMoButton_OnDestroy_mE8D51330ED641B12795E984B6F0ECCB00536DBDB (void);
// 0x00000056 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::ChangeSpeed()
extern void SlowMoButton_ChangeSpeed_m0C19576C039AA7BBBC478C87A6C964376BA58EAB (void);
// 0x00000057 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::.ctor()
extern void SlowMoButton__ctor_m993A85A43C7462445119B63636FD65A6F8C0F6BF (void);
static Il2CppMethodPointer s_methodPointers[87] = 
{
	MenuSceneLoader_Awake_m18B4E09AAFDC9578469D066C50A8AC0054AA34AC,
	MenuSceneLoader__ctor_m27FD45CA6C6C8B579D2FA4EEDABBA35F2C7EF6BC,
	PauseMenu_Awake_mB04BCBDA84AEC654D1976EC3DF61A0CF68D2C86C,
	PauseMenu_MenuOn_m8973DC956DD5235381EE4ACB4A182AA5BF0EF2EA,
	PauseMenu_MenuOff_mCD1AF41F916B0C02D2FD6F82377DBEAFF7C30720,
	PauseMenu_OnMenuStatusChange_mD1E8BD9B9CCB274A83FFF0FE2E06E6ABD0361B4A,
	PauseMenu_Update_m191CABDC11442A2CC104FC8B3244D04826E7BD57,
	PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018,
	SceneAndURLLoader_Awake_m8F276157A2A5FA943EF7918D6CCDB81273317E23,
	SceneAndURLLoader_SceneLoad_m2B09BD48F419F49A6BD461DBC7B2290EC8632B06,
	SceneAndURLLoader_LoadURL_m47E3E286E80F2D5B3E6A164C32F7E1B473532AE2,
	SceneAndURLLoader__ctor_m6DEE574FADF9E3E894594690CB2755F69D5D4BE5,
	CameraSwitch_OnEnable_mD422991EDD9D880928644EE1BC4E557EE644679C,
	CameraSwitch_NextCamera_m38AB4521C129032FA1DA5154E09D36D0FE2DB257,
	CameraSwitch__ctor_m550FCA9B0C24BBBC8BDBCAAAAA7BBF26312399FE,
	LevelReset_OnPointerClick_m9625C8235343CB1DE6B089AD6F2D5ACF738072A4,
	LevelReset__ctor_mFD2BB9AF1A9D7E795DDAAE2F33C1F0EB1FB31F07,
	AudioManager_Start_m54C0A7ACBAB2F38052C6B900BBBC3261339662FC,
	AudioManager_Update_mC2BFAF2A1E9F96A9BA1C48BBBBB1ED9361E537C8,
	AudioManager_ChangeBGM_m73D374AA2B395FE75D801E3EADCFE34F788381F1,
	AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD,
	Controller_Start_mD256D29928E12349C2565BD046108EE5A3537157,
	Controller_Update_mBC27223743238412D4F6FF7B8B3E3FB2DF47F1A0,
	Controller_OnObsticaleEnter_mBA86B4C82A0289681B3F5041889E79DDFD674845,
	Controller__ctor_m50D06C2CAE9B5496780A83ABCF68C529A44EE9F7,
	GameBehaviour_Start_m598107700AD6F8F7AFC2FF3C892721AB45757752,
	GameBehaviour_Update_m4F6DE76D4E1772CB1D372F80056C7A911001F495,
	GameBehaviour_OnCollisionEnter_m81B9B70DD60B5FF20E1BBF24B0F431F0323DE77E,
	GameBehaviour_OnTriggerEnter_m9105655E75F774B9F6EB92F2959F2DC2AD113887,
	GameBehaviour__ctor_m3B3CE215E6DE80D656F1AF405CEA4AA8458CDB53,
	LevelText_Start_m03232956C7BBDED2C383AD8B7ACADC3E2C672907,
	LevelText_OnTriggerEnter_mCF6FE1032791BF97F6B623F059F3F40B9C23715A,
	LevelText_WaitForSec_m33F5F7E50AFE3688DE6FFB3A7EC06DD0BCFED8D9,
	LevelText_Update_mED2776800099C8816820DE02C80C1EAED91C559E,
	LevelText__ctor_m14A5F02DDB0F576F5281310DD7245B2CDBC41FE9,
	U3CWaitForSecU3Ed__3__ctor_m7B4302C3D8A7CFDF02F534BF5CCDB4D7BCEB4422,
	U3CWaitForSecU3Ed__3_System_IDisposable_Dispose_m287F9796B41BD6A8D01CDCDDC89C9AC78F6EE2D3,
	U3CWaitForSecU3Ed__3_MoveNext_m24C5DDA54B9822205EF34FB9BCDF50CCC3EE4631,
	U3CWaitForSecU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6953009194EDF55B5668DC359C2F5CE90884E786,
	U3CWaitForSecU3Ed__3_System_Collections_IEnumerator_Reset_m1ABAF71CE03CF0021D613B1792AEC15BE3246D11,
	U3CWaitForSecU3Ed__3_System_Collections_IEnumerator_get_Current_m36C7A048FBF14E8A3441F42FE1FD344328DC252B,
	MouseLooker_Start_m62C65D9E72654C73653B641DE577A0ED93A3D437,
	MouseLooker_Update_m9B2049DFCDBC73BA481BA00988231B1011C28E55,
	MouseLooker_LockCursor_mB30F16B0D059038F92963F3A9916213DC59A1E70,
	MouseLooker_LookRotation_mDBD54C8212E2489D0C18C35F277ABE6CDDDC171B,
	MouseLooker_ClampRotationAroundXAxis_mCF88B2CD1B917D1E9EFB8275F6D14D63C67D7C56,
	MouseLooker__ctor_m57240C9A9D42D49929076F9A34D4CD8A4E8C9B98,
	StartingText_Start_mC3210F7E53E01202B4F67D1E910A2676381C54C4,
	StartingText_Update_mF81F252B33E8F5CCA269DA4645390D0125EA1BE3,
	StartingText__ctor_m6B3571AA22D40B7A19870BCCAC1B828A2FCDA146,
	SwitcMusicTrigger_Start_m0AF0CCE555C762F7C425E8635E7A4994F1A21A67,
	SwitcMusicTrigger_Update_m8391FA6CCF87354CF1AB61E82EC21C2C72D07F13,
	SwitcMusicTrigger_OnTriggerEnter_m8AC6EADB8699B34F93230DE17CFB8BCBC3DD3AFC,
	SwitcMusicTrigger__ctor_m3108B429117F1F1BDE14236992FA0C4F592D84C5,
	Teleport_OnTriggerEnter_mD257053A6E90223E0B71BD2181254C9D4F244017,
	Teleport_Start_m09C6BA2D5A92B3DB4BC25D9BAB55557CC3A64E64,
	Teleport_Update_mBAF587BC3352BB595BF3C7FBC9E2F036B8628063,
	Teleport__ctor_mF232DF6C23A42F13C1B57881E696EBA352801E3B,
	TutorialText_Start_mD882F1E8E8DD370A4FD4474E9AE41738E16AB501,
	TutorialText_OnTriggerEnter_mEDFA8342059AF1EC688AA926E8D98351B7DDF7D1,
	TutorialText_WaitForSec_mDC285189613F59023A273280C8F9A6768580884E,
	TutorialText_Update_m3070918573AECA5D6F7E51C8DF1CB5300A76A022,
	TutorialText__ctor_mD0EF3C12D39D80C2E531FC9A1BE098E196B710E9,
	U3CWaitForSecU3Ed__3__ctor_m7D06D878043B1BF25D0BF4F2CD7E65C6ED7DCD9A,
	U3CWaitForSecU3Ed__3_System_IDisposable_Dispose_mE7FE26E8AB05EC1423B5F249403F41883A8BD20F,
	U3CWaitForSecU3Ed__3_MoveNext_m431B15DDDF652E339C2BFCDDCA8E97EF09EEFAED,
	U3CWaitForSecU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B2144C612882471124DAA52DC6D8FB851C545B2,
	U3CWaitForSecU3Ed__3_System_Collections_IEnumerator_Reset_m3DBCCB9BDBF62F25BC11015E32689B626953B40D,
	U3CWaitForSecU3Ed__3_System_Collections_IEnumerator_get_Current_m0A71CC4BCF8DCD7244A62D5124A3DD431E0F3983,
	ParticleSceneControls_Awake_m3268C5B5B81D4EE59C85C27AED5CA3956C284DA4,
	ParticleSceneControls_OnDisable_mEB5EAF7BC1928EBEFD83E651459327948255D811,
	ParticleSceneControls_Previous_mC2FE564898A6019238C5611E63B5EC3581CEAA63,
	ParticleSceneControls_Next_m4AB6C18CBE0148C4D354FDCAD5EE45D472577B8E,
	ParticleSceneControls_Update_m7B5894AE70D3733C901BFFF529195AC1467D632B,
	ParticleSceneControls_KeyboardInput_m29C494EC6ACF4EE139686EF47C3A167D1007970F,
	ParticleSceneControls_CheckForGuiCollision_mB8EDC5E34CE1B59986991852009A011D9514A8FB,
	ParticleSceneControls_Select_m123BA8121C7C5C454EF649A13C724F2622B4517A,
	ParticleSceneControls__ctor_mFE76B488C636F213B77193EDFF38F514A3186631,
	ParticleSceneControls__cctor_mBA868BA9BC2400AD1AB420B3D0AB42D863358338,
	DemoParticleSystem__ctor_mA046B73C934441755CA5D57991D7C453645AD8CE,
	DemoParticleSystemList__ctor_mAFBB16272EB2E695826211DD030553401DF6E83B,
	PlaceTargetWithMouse_Update_m28E68FE68FA0185C0D20A6001F4C262868348BE3,
	PlaceTargetWithMouse__ctor_m3582FA7DC3CF8198244F9EB014FE12AC97985FE7,
	SlowMoButton_Start_m8F334286C4BEE772EAA4842F431E01BB4929A04C,
	SlowMoButton_OnDestroy_mE8D51330ED641B12795E984B6F0ECCB00536DBDB,
	SlowMoButton_ChangeSpeed_m0C19576C039AA7BBBC478C87A6C964376BA58EAB,
	SlowMoButton__ctor_m993A85A43C7462445119B63636FD65A6F8C0F6BF,
};
static const int32_t s_InvokerIndices[87] = 
{
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1019,
	1019,
	1194,
	1194,
	1194,
	1194,
	1019,
	1194,
	1194,
	1194,
	1019,
	1194,
	1194,
	1194,
	1019,
	1194,
	1194,
	1194,
	1019,
	1019,
	1194,
	1194,
	1019,
	1161,
	1194,
	1194,
	1009,
	1194,
	1181,
	1161,
	1194,
	1161,
	1194,
	1194,
	1036,
	1194,
	831,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1019,
	1194,
	1019,
	1194,
	1194,
	1194,
	1194,
	1019,
	1161,
	1194,
	1194,
	1009,
	1194,
	1181,
	1161,
	1194,
	1161,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1181,
	1009,
	1194,
	2048,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	87,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
